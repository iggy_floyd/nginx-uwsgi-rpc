# @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de
# simple makefile to manage this project


all: configuration doc README.wiki test configure


# to check that the system has all needed components
configuration: configure
	@./configure


# to check an update in the configure.ac. If it's found, update the 'configure' script.
configure: configure.ac
	@./configure.ac



doc: README.wiki 

	-@ mkdir doc
	-@ ls README.wiki | sed -ne 's/.wiki//p' | xargs -I {}  echo "wiki-tool/mediawiki2texi.py {}.wiki {}.info {} >{}.texinfo; makeinfo --force --html {}.texinfo; makeinfo {}.texinfo; cat {}.info" | sh
	-@ rm *info
	-@ ls README.wiki | sed -ne 's/.wiki//p' | xargs -I {}  echo "cp {}/index.html doc; rm -r {}" | sh


run: configuration test

	- echo "Not implemented"



install_nginx:
	cd nginx-1.6.0; pwd; ./configure --prefix=`pwd`/../nginx_build; make; make install;
	cp nginx.conf nginx_build/conf/nginx.conf


start_nginx:
	nginx_build/sbin/nginx


stop_nginx:
	nginx_build/sbin/nginx -s stop


# to clean all temporary stuff
clean: stop_nginx
	-@rm -r config.log autom4te.cache
	-cd nginx-1.6.0; make clean
	-@rm -r nginx_build/*



.PHONY: configuration clean all doc run test install_nginx start_nginx stop_nginx
