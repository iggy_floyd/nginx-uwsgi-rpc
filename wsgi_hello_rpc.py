import json
from uwsgidecorators import rpc 
import uwsgi


@rpc('hello')  
def hello(test_param):
    return json.dumps({'Hello World from':test_param})

#uwsgi.register_rpc("hello", hello)
