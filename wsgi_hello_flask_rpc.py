import json
from flask import Flask
from flask_jsonrpc import JSONRPC



# Flask application
app = Flask(__name__)

# Flask-JSONRPC
jsonrpc = JSONRPC(app, '/hello', enable_web_browsable_api=True)


# An 'index' method
@jsonrpc.method('App.index')
def index():
    return 'Welcome to Flask JSON-RPC'


# A 'Hello World' method
@jsonrpc.method('App.hello_world')  
def hello_world(test_param):
    print test_param
    return json.dumps({'Hello World from':test_param})





if __name__ == '__main__':
    app.run(host='127.0.0.1',port=8080, debug=True)
